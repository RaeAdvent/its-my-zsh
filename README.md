# ITS-MY-ZSH #

A simple oh-my-zsh settings for web developers to get you up and running.

### What is this repository for? ###

* A simple oh-my-zsh settings for web developers
* 1.0
* [oh-my-zsh](https://ohmyz.sh/)

### Activated/Installed plugins? ###

* git
* zsh-autosuggestion
* zsh-autocomplete
* zoxide
* fzf
* extra aliases
